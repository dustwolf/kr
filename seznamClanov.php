<?php

require_once "html.php";
require_once "mysqli.php";

$dokument = new html();

$baza = new dblink();

$clani = $baza->q("
 SELECT `stevilka`, `ime`, `priimek`, `datumPristopa`
   FROM `clani`
");

?>
<table class="table">
 <thead>
  <tr>
   <th>Št.</th>
   <th>Ime</th>
   <th>Priimek</th>
   <th>Datum Pristopa</th>
  <tr>
 </thead>
 <tbody>
<?php
 foreach($clani as $clan) {
  $datumPristopa = strtotime( $clan["datumPristopa"] );
?>
  <tr>
   <td><?php echo $clan["stevilka"]; ?></td>
   <td><?php echo $clan["ime"]; ?></td>
   <td><?php echo $clan["priimek"]; ?></td>
   <td><?php echo date("d. n. o.",  $datumPristopa ); ?></td>
  </tr>
<?php
 }
?>
 </tbody>
</table>

