<?php

require_once "../mysqli.php";
$db = new dblink();

//ČE SE NE GRE POVEZATI, SE PRIJAVI Z ROOTOM IN IZDELAJ BAZO IN UPORABNIKA
if($db->link->connect_error) {

 $db = new dblink("","root",$_GET["pw"]);

 $db->q("
  CREATE DATABASE `knjiznica`
 ");

 $db->q("
  CREATE USER 'knjiznica'@'localhost' IDENTIFIED BY 'knjiga';
 ");

 $db->q("
  GRANT ALL PRIVILEGES ON `knjiznica`.* TO 'knjiznica'@'localhost'
 ");

 $db->q("
  FLUSH PRIVILEGES
 ");

}

$db = new dblink();

//PREVERI ČE OBSTAJAJO TABELE
/*
$tmp = $db->q("
 SHOW TABLES LIKE 'clani'
");

if(!isset($tmp[0])) {
*/

 $db->q("
  CREATE TABLE `clani` (
  `ime` varchar(25) DEFAULT NULL,
  `priimek` varchar(25) DEFAULT NULL,
  `stevilka` int(11) NOT NULL,
  `darovanPredmet` varchar(50) DEFAULT NULL,
  `datumPristopa` date DEFAULT NULL,
  `trajanjeClanstva` date DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `postniNaslov` text,
  `telefon` varchar(25) DEFAULT NULL,
  `letoRojstva` year(4) DEFAULT NULL,
  PRIMARY KEY (`stevilka`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8
 ");

/*
}

$tmp = $db->q("
 SHOW TABLES LIKE 'inventar'
");

if(!isset($tmp[0])) {
*/

 $db->q("
  CREATE TABLE `inventar` (
  `koda` int(11) NOT NULL,
  `kategorija` int(11) DEFAULT NULL,
  `naziv` varchar(45) DEFAULT NULL,
  `predvidenCasIzposoje` int(11) DEFAULT NULL,
  `ocenjenaVrednost` int(11) DEFAULT NULL,
  `kavcija` tinyint(1) DEFAULT NULL,
  `donator` int(11) DEFAULT NULL,
  `datumSprejema` date DEFAULT NULL,
  `znamkaSprejema` varchar(45) DEFAULT NULL,
  `posebnosti` text,
  `primerenZaIzposojo` tinyint(1) DEFAULT NULL,
  `rezervniDel` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`koda`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8
 ");

//}

//TODO popravi tabele da bodo ustrezale trenutni obliki


?>
