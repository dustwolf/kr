<?php

class html {

 function __construct($naslov = "") {
?><!DOCTYPE html>
<html>
 <head>
  <meta charset="UTF-8">
  <title><?php $naslov; ?></title>
  <link rel="stylesheet" href="bootstrap.min.css">
  <link rel="stylesheet" href="bootstrap-theme.min.css">
  <script src="jquery-1.11.2.min.js"></script>
 </head>
 <body>
<?php
 }

 function __destruct() { 
?>
  <script src="bootstrap.min.js"></script>
 </body>
</html> 
<?php
 }

}

?>
