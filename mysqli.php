<?php

class dblink {

 public $link;

 function __construct($database = "knjiznica", $user = "knjiznica", $pass = "knjiga", $host = "127.0.0.1") {

  $this->link = new mysqli($host, $user, $pass, $database);
  $this->link->set_charset("utf8");

 }
 
 function q($q, $primary = "") {
    
  $r = $this->link->query($q);
  echo $this->link->error;

  if($r !== True && $r !== False) {
   while($tmp = $r->fetch_assoc()) {
    if($primary == "") {
     $results[] = $tmp;
    } else {
     $results[$tmp[$primary]] = $tmp;
    }
   }
   return $results;
  } else {
   return $this->link->num_rows;
  }

 }
 
 function e($e) {
  return $this->link->real_escape_string($e);
 }

 function __destruct() {
  $this->link->close();
 }

}

?>
