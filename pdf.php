<?php

require_once "html.php";

class pdf {

 private $aplikacija;
 private $naslov;

 private $glava;
 private $noga;

 private $vsebina;
 
 function __construct($aplikacija = "pdf", $naslov = "Izpis") {
 
  //NASTAVI NASLOVE
  $this->aplikacija = $aplikacija;
  $this->naslov = $naslov;

  //NA ZAČETKU TI ŠE NISO BILI IZVEDENI
  $this->glava = '';
  $this->noga = '';  
  
  //ZAČNI HTML VSEBINE
  ob_start();
   $this->vsebina = new html($this->aplikacija."-vsebina");
 }

 function glava($html = "") {

  //ČE GLAVA ŠE NI DEFINIRANA (sicer bi skripta za sabo pustila temp file)
  if($this->glava == "") {

   //DEFINIRAJ TEMP FILE
   $this->glava = "/tmp/".uniqid($this->aplikacija."-glava").".html";

   //POSNAMI VSEBINO V HTML FILE
   ob_start();
    $d = new html($this->aplikacija."-glava");
     echo $html;
    unset($d);
   file_put_contents($this->glava, ob_get_clean());   
  } 

 }

 function noga($html = "") {

  //ČE NOGA ŠE NI DEFINIRANA (sicer bi skripta za sabo pustila temp file)
  if($this->noga == "") {

   //DEFINIRAJ TEMP FILE
   $this->noga = "/tmp/".uniqid($this->aplikacija."-noga").".html";

   //POSNAMI VSEBINO V HTML FILE
   ob_start();
    $d = new html($this->aplikacija."-noga");
     echo $html;
    unset($d);
   file_put_contents($this->noga, ob_get_clean());   
  } 

 }

 function __destruct() {

  //ZAKLJUČI HTML VSEBINE IN POSNAMI OUTPUT V TEMP FILE
  unset($this->vsebina);
  $html = ob_get_clean();
  $file = "/tmp/".uniqid($this->aplikacija."-vsebina").".html";
  file_put_contents($file, $html);

  //ČE GLAVA ŠE NI DEFINIRANA
  if($this->glava == "") {
   $this->glava();
  }

  //ČE NOGA ŠE NI DEFINIRANA
  if($this->noga == "") {
   $this->noga();
  }

  //RENDRAJ PDF
  header('Content-Type: application/pdf');
  header('Content-Disposition: inline; filename="'.$this->naslov.'.pdf"');
  header('Content-Transfer-Encoding: binary');
  passthru("/usr/local/bin/wkhtmltopdf --title ".escapeshellarg($this->naslov)." --header-spacing 2 --header-html ".escapeshellarg($this->glava)." --footer-html ".escapeshellarg($this->noga)." ".escapeshellarg($file)." -");
  
  //POBRIŠI ZAČASNE DATOTEKE
  unlink($file);
  unlink($this->glava);
  unlink($this->noga);
 }

}

?>
